Dieser Ordner ist Teil der Abgabe der Wissenschaftlichen Zulassungsarbeit "Gauß-Kollokationsverfahren und ihr Normalformen". Er beinhaltet die mit deal.II in Anlehnung an das dort implementierte Theta-Verfahren erstellten Codes, die in der Arbeit verwendet wurden. Alle in der Arbeit dargestellten Ergebnisse sind mit diesen Codes gewonnen worden. 
Ein Beispiel beinhaltet immer eine .cc-Datei und eine .h-Datei gleichen Namens, um das Beispiel auszuführen muss deal.II installiert werden.
Eine Kopie kann beispielsweise über die Clone-Funktion erstellt werden.

Link zur deal.II Homepage: http://dealii.org/

Link zur Dokumentation des Theta-Verfahrens: http://dealii.org/8.3.0/doxygen/deal.II/classAlgorithms_1_1ThetaTimestepping.html#a97c5f106de3c6cae044e86b2d9e60b6d