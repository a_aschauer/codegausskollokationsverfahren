 
// header datei bestehend aus angepasstem theta_timestepping_templates.h
// sechsstufiges Gauß-Kollokationsverfahren


#include <deal.II/base/parameter_handler.h>
#include <deal.II/lac/vector_memory.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/algorithms/theta_timestepping.h>


DEAL_II_NAMESPACE_OPEN

namespace Algorithms {
  template <class VECTOR>
  ThetaTimestepping<VECTOR>::ThetaTimestepping (Operator<VECTOR> &e, Operator<VECTOR> &i)
    : vtheta(0.5), adaptive(false), op_explicit(&e), op_implicit(&i)
  {}
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::notify(const Event &e)
  {
    op_explicit->notify(e);
    op_implicit->notify(e);
  }
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::declare_parameters(ParameterHandler &param)
  {
    param.enter_subsection("ThetaTimestepping");
    TimestepControl::declare_parameters (param);
    param.declare_entry("Theta", ".5", Patterns::Double());
    param.declare_entry("Adaptive", "false", Patterns::Bool());
    param.leave_subsection();
  }
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::parse_parameters (ParameterHandler &param)
  {
    param.enter_subsection("ThetaTimestepping");
    control.parse_parameters (param);
    vtheta = param.get_double("Theta");
    adaptive = param.get_bool("Adaptive");
    param.leave_subsection ();
  }

  /*
   * Funktion zur Berechnung des Kronecker Produkts zweier FullMatrix Objekte
   * function that computes the kronecker product of two FullMatrix objects
   */
  FullMatrix<double> kronecker(const FullMatrix<double> &a, const FullMatrix<double> &b) 
  {
    FullMatrix<double> axb(a.m()*b.m(), a.n()*b.n());

    for(unsigned int i=0; i<a.m(); ++i)
      for(unsigned int j=0; j<a.n(); ++j)
	 axb.add(b, a(i,j), i*b.m(), j*b.n());

    return axb;
  }
  
  /*
   * Einheitsmatrix der Dimension n=2
   * identiy matrix of size n=2
   */
  FullMatrix<double> identity_matrix_size2(IdentityMatrix(2));
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::operator() (AnyData &out, const AnyData &in)
  {
    Assert(!adaptive, ExcNotImplemented());
    
    deallog.push ("Gauss-Kollokation_St6"); 
    deallog.precision(16);
    
    
    int odesize = 2; //Dimension der ODE-Matrix
    
    /*
     * Werte des Gauß-Kollokationsverfahrens (s=6)
     * berechnet mit Octave
     */
      
    /*
     * Transformationsmatrix T der Jordannormalform von A_invers
     */
    FullMatrix<double> jordan_trafo_matrix(6);
    jordan_trafo_matrix(0,0) = -0.001957637767504;
    jordan_trafo_matrix(0,1) = -0.015523884904473;
    jordan_trafo_matrix(0,2) = 0.005465652381252;
    jordan_trafo_matrix(0,3) = -0.002025179859690;
    jordan_trafo_matrix(0,4) = 0.002245832983852;
    jordan_trafo_matrix(0,5) = 0.003028135343733;
    jordan_trafo_matrix(1,0) = 0.009685786713440;
    jordan_trafo_matrix(1,1) = 0.030662337246230;
    jordan_trafo_matrix(1,2) = -0.006049334769087;
    jordan_trafo_matrix(1,3) = 0.006696383994634;
    jordan_trafo_matrix(1,4) = -0.002802969510442;
    jordan_trafo_matrix(1,5) = -0.001602058553936;
    jordan_trafo_matrix(2,0) = -0.045557282850795;
    jordan_trafo_matrix(2,1) = -0.053353030317697;
    jordan_trafo_matrix(2,2) = -0.007629843987545;
    jordan_trafo_matrix(2,3) = -0.005574176089584;
    jordan_trafo_matrix(2,4) = -0.000880797976738;
    jordan_trafo_matrix(2,5) = 0.008076216127248;
    jordan_trafo_matrix(3,0) = 0.172856714708811;
    jordan_trafo_matrix(3,1) = 0.019069352853177;
    jordan_trafo_matrix(3,2) = -0.021776505510247; 
    jordan_trafo_matrix(3,3) = -0.063546793662219;
    jordan_trafo_matrix(3,4) = -0.043552536809041;
    jordan_trafo_matrix(3,5) = 0.025902104391323;
    jordan_trafo_matrix(4,0) = -0.171740767682272;
    jordan_trafo_matrix(4,1) = 0.403771048921062;
    jordan_trafo_matrix(4,2) = 0.261607449795212;
    jordan_trafo_matrix(4,3) = -0.227098256980160;
    jordan_trafo_matrix(4,4) = -0.289780714133837;
    jordan_trafo_matrix(4,5) = 0.073038588926935;   
    jordan_trafo_matrix(5,0) = -0.878083217086105;
    jordan_trafo_matrix(5,1) = 0.;
    jordan_trafo_matrix(5,2) = 0.935559092753478;
    jordan_trafo_matrix(5,3) = 0.;
    jordan_trafo_matrix(5,4) = -0.952908244322661;
    jordan_trafo_matrix(5,5) = 0.;



    /*
     * berechne das Kronecker Produkt der Transformationsmatrix und Einheitsmatrix der Dimension n=2
     * T x I
     */
    FullMatrix<double> trafo_matrix_kron_id;
    trafo_matrix_kron_id = kronecker(jordan_trafo_matrix, identity_matrix_size2);
      
    /*
     * invertiere die Transformationsmatrix
     */
    FullMatrix<double> invers_trafo_matrix(6);
    invers_trafo_matrix.invert<double>(jordan_trafo_matrix);
    
    /*
     * berechne das Kronecker Produkt der Inversen der Transformationsmatrix und Einheitsmatrix der Dimension n=2
     * T^(-1) x I
     */
    FullMatrix<double> invers_trafo_matrix_kron_id;
    invers_trafo_matrix_kron_id = kronecker(invers_trafo_matrix, identity_matrix_size2);
    
    /*
     * Jordannormalform der Inversen der RK-Matrix A
     */
    FullMatrix<double> jordan_A_invers(6);
    jordan_A_invers(0,0) = 5.03186449562150;   
    jordan_A_invers(0,1) = 8.98534590730802;
    jordan_A_invers(1,0) = -8.98534590730802;
    jordan_A_invers(1,1) = 5.03186449562150;
    jordan_A_invers(2,2) = 7.47141671265155;
    jordan_A_invers(2,3) = 5.25254462289429;
    jordan_A_invers(3,2) = -5.25254462289429;
    jordan_A_invers(3,3) = 7.47141671265155;  
    jordan_A_invers(4,4) = 8.49671879172691;
    jordan_A_invers(4,5) = 1.73501934646266;
    jordan_A_invers(5,4) = -1.73501934646266;
    jordan_A_invers(5,5) = 8.49671879172691;  
    
    /*
     * berechne das Kronecker Produkt der JNF von A_invers und Einheitsmatrix der Dimension n (=2)
     */
    FullMatrix<double> infAj_kron_id;
    infAj_kron_id = kronecker(jordan_A_invers, identity_matrix_size2);
    
    /*
     * lese die 2nx2n Blöcke des gerade berechneten Kronecker Produkts aus (hier 4x4)
     */
    FullMatrix<double> block1(2*odesize);
    FullMatrix<double> block2(2*odesize);
    FullMatrix<double> block3(2*odesize);
    
    for (int i = 0; i < 2*odesize; i++)
    {
      for (int j =0; j < 2*odesize; j++)
      {
	block1(i,j) = infAj_kron_id(i,j);
	block2(i,j) = infAj_kron_id(i+(2*odesize),j+(2*odesize));
	block3(i,j) = infAj_kron_id(i+(4*odesize),j+(4*odesize));
      }
    }
    
    /*
     * Werte d_i zur Berechnung der neuen Iterierten 
     * d = RK_Gewichte*A_invers
     */
    Vector<double> values_d_i(6);
    values_d_i(0) = -1.620385924479386;
    values_d_i(1) = 1.132262857214359;
    values_d_i(2) = -0.996157757544450;
    values_d_i(3) = 0.996157757544725;
    values_d_i(4) = -1.132262857214125;
    values_d_i(5) = 1.620385924479464;   

                


    /*
     * Vektoren, die zu den AnyData Objekten hinzugefügt werden
     * die Pointer-Struktur ist von Theta_Timestepping übernommen
     */

    /*
     * Vektor, der den Startwert bekommt und der später immer die Lösung des expliziten Teils (erster 2n dimensionaler Vektor) beinhaltet
     */
    VECTOR &solution = *out.entry<VECTOR *>(0);
    GrowingVectorMemory<VECTOR> mem1;
    typename VectorMemory<VECTOR>::Pointer aux1(mem1);
    aux1->reinit(solution);
   
    /* 
     * Vektor, in dem der zweite berechnete 2n-dimensionale Vektor des expliziten Teils gespeichert wird (Dimension 4)
     */
    VECTOR store(2*odesize);
    GrowingVectorMemory<VECTOR> mem2;
    typename VectorMemory<VECTOR>::Pointer aux2(mem2);
    aux2->reinit(store);
    
    /*
     * dritter Vektor
     */
    VECTOR store1(2*odesize);
    GrowingVectorMemory<VECTOR> mem3;
    typename VectorMemory<VECTOR>::Pointer aux3(mem3);
    aux3->reinit(store1);
    
    /*
     * 3 Vektoren, in denen die drei Teile der mit T^-1 x I multiplizierten alten Iterierten gespeichert werden
     * Größe der Vektoren ist egal, da sie sowieso mit Vektoren der passenden Größe überschrieben werden
     */
    VECTOR store_shift1(2);
    GrowingVectorMemory<VECTOR> mem_shift1;
    typename VectorMemory<VECTOR>::Pointer aux_shift1(mem_shift1);
    aux_shift1->reinit(store_shift1);
    
    VECTOR store_shift2(2);
    GrowingVectorMemory<VECTOR> mem_shift2;
    typename VectorMemory<VECTOR>::Pointer aux_shift2(mem_shift2);
    aux_shift2->reinit(store_shift2);
    
    VECTOR store_shift3(2);
    GrowingVectorMemory<VECTOR> mem_shift3;
    typename VectorMemory<VECTOR>::Pointer aux_shift3(mem_shift3);
    aux_shift3->reinit(store_shift3);
    
    /*
     * erstelle drei Vektoren um später die Z_Stern Teilvektoren zu speichern, die der implizite Löser berechnet
     * Vektoren gleich auf Dimension 2n setzen, dann muss die Dimension nicht mehr angepasst werden (hier Dimension 4)
     */
    VECTOR output_zstar1(2*odesize);
    GrowingVectorMemory<VECTOR> mem_z_star1;
    typename VectorMemory<VECTOR>::Pointer aux_z_star1(mem_z_star1);
    aux_z_star1->reinit(output_zstar1);
    
    VECTOR output_zstar2(2*odesize);
    GrowingVectorMemory<VECTOR> mem_z_star2;
    typename VectorMemory<VECTOR>::Pointer aux_z_star2(mem_z_star2);
    aux_z_star2->reinit(output_zstar2);
    
    VECTOR output_zstar3(2*odesize);
    GrowingVectorMemory<VECTOR> mem_z_star3;
    typename VectorMemory<VECTOR>::Pointer aux_z_star3(mem_z_star3);
    aux_z_star3->reinit(output_zstar3);    
    
    control.restart();
    
    d_explicit.time = control.now();
      
    	
    /*
     * in src1 stehen die Daten, die benutzt werden um den expliziten Teil zu berechnen
     */
    AnyData src1;
    /*
     * die ersten drei Vektoren werden später mit der davor mit T^-1 x I multiplizierten und in Vektoren der Dimension 2n geteilten alten Iterierten befüllt
     * damit das überschreiben klappt dürfen die Vektoren nicht const sein
     */
    src1.add<VECTOR *>(aux_shift1, "shifted prev iterate 1");
    src1.add<VECTOR *>(aux_shift2, "shifted prev iterate 2");
    src1.add<VECTOR *>(aux_shift3, "shifted prev iterate 3");
    src1.add<const VECTOR *>(&solution, "Previous iterate");
    src1.add<const double *>(&d_explicit.time, "Time");
    src1.add<const double *>(&d_explicit.step, "Timestep");
    
    /*
     * Output des expliziten Lösers
     */
    AnyData out1;
    out1.add<VECTOR *>(aux1, "Solution");
    out1.add<VECTOR *>(aux2, "Solution2");
    out1.add<VECTOR *>(aux3, "Solution3");

    /*
     * src2: Daten für den impliziten Löser
     */
    AnyData src2;
    /*
     * ersten drei Vektoren bekommen das im expliziten Teil Berechnete
     */
    src2.add<const VECTOR *>(aux1, "Previous time 1"); 
    src2.add<const VECTOR *>(aux2, "Previous time 2"); 
    src2.add<const VECTOR *>(aux3, "Previous time 3"); 
    /*
     * übergebe die Matrix Blöcke 
     */
    src2.add<FullMatrix<double> *>(&block1, "Block1"); 
    src2.add<FullMatrix<double> *>(&block2, "Block2"); 
    src2.add<FullMatrix<double> *>(&block3, "Block3"); 
    src2.add<const double *>(&d_implicit.time, "Time");
    src2.add<const double *>(&d_implicit.step, "Timestep");
        
    /*
     * Output des impliziten Lösers, mit dem dann weitergearbeitet wird
     */
    AnyData out_zstar;
    out_zstar.add<VECTOR *>(aux_z_star1, "Z stern1");
    out_zstar.add<VECTOR *>(aux_z_star2, "Z stern2");
    out_zstar.add<VECTOR *>(aux_z_star3, "Z stern3");
    
    if (output != 0)
    (*output) << 0U << out;
    
    /*
     * for Schleife, die zuerst die alte Iterierte mit T^-1 x I multipliziert, den expliziten und impliziten Löser aufruft und dann aus den berechneten Vektoren die neue Iterierte berechnet
     */    
    for (unsigned int count = 1; d_explicit.time < control.final(); ++count)
    {
      const bool step_change = control.advance();
      d_implicit.time = control.now();
      //default stepsize: 0.01
      d_explicit.step = control.step();
      d_implicit.step = control.step();
      
     
      deallog << "Timestep:" << d_implicit.time << std::endl;
            
     
      op_explicit->notify(Events::new_time);
      op_implicit->notify(Events::new_time);
      if (step_change)
	{
	  op_explicit->notify(Events::new_timestep_size);
	  op_implicit->notify(Events::new_timestep_size);
	}
	

      /*
       * speichere die alte Iterierte für spätere Berechnungen
       */
      VECTOR take_prev_iterate = *src1.entry<const VECTOR *>(3);
           
      /*
       * verlängere die alte Iterierte auf die Dimension s*n = 12
       */
 
      VECTOR long_prev_iterate(6*odesize);
      for (int i = 0; i < odesize; i++)
      {
	long_prev_iterate(i) = take_prev_iterate(i);
	long_prev_iterate(i+odesize) = take_prev_iterate(i);
	long_prev_iterate(i+(2*odesize)) = take_prev_iterate(i);
	long_prev_iterate(i+(3*odesize)) = take_prev_iterate(i);
	long_prev_iterate(i+(4*odesize)) = take_prev_iterate(i);
	long_prev_iterate(i+(5*odesize)) = take_prev_iterate(i);
      }
      /*
       * berechne T^-1 x I mal die verlängerte alte Iterierte, teile das Ergebnis in drei Vektoren der Dimension 2n und schreibe sie in src1 (Dimension hier 4)
       */ 
      VECTOR shifted_prev_it(6*odesize);
      invers_trafo_matrix_kron_id.vmult<double>(shifted_prev_it, long_prev_iterate);
     
      VECTOR shifted_prev_it1(2*odesize);
      VECTOR shifted_prev_it2(2*odesize);
      VECTOR shifted_prev_it3(2*odesize);
  
      for (int i = 0; i < 2*odesize; i++)
      {
	shifted_prev_it1(i) = shifted_prev_it(i);
	shifted_prev_it2(i) = shifted_prev_it(i+(2*odesize));
	shifted_prev_it3(i) = shifted_prev_it(i+(4*odesize));
      }
      //speichern
      *src1.entry<VECTOR *>(0) = shifted_prev_it1;   
      *src1.entry<VECTOR *>(1) = shifted_prev_it2; 
      *src1.entry<VECTOR *>(2) = shifted_prev_it3; 
      
      /*
       * passe die Dimension der Vektoren an, die noch nicht stimmt (nur der Vektor, in den der erste 2n dimensionale Teil des Ergebnisses des expliziten Teils gespeichert wird)
       */ 
      VECTOR long_solution(2*odesize);
      *out1.entry<VECTOR *>(0) = long_solution;
     
     /*
      * rufe expliziten und impliziten Löser auf
      */
      (*op_explicit)(out1, src1);      
      (*op_implicit)(out_zstar, src2);
      
      
      /*
       * speichere die Ausgabe des impliziten Lösers, um dann damit weiterzurechnen
       */
      VECTOR output_block1 = *out_zstar.entry<VECTOR *>(0);
      VECTOR output_block2 = *out_zstar.entry<VECTOR *>(1);
      VECTOR output_block3 = *out_zstar.entry<VECTOR *>(2);
      
      /*
       * erstelle mit den Werten einen Vektor der Dimension s*n=12, um diesen mit TxI zu multiplizieren
       */
      VECTOR output_full(6*odesize);
      for (int i = 0; i < 2*odesize; i++)
      {
	output_full(i) = output_block1(i);
        output_full(i+(2*odesize)) = output_block2(i);
	output_full(i+(4*odesize)) = output_block3(i);
	
      }

      /*
       * multipliziere mit T x I, speichere das Ergebnis in z_values
       */
      VECTOR z_values(6*odesize);
      trafo_matrix_kron_id.vmult<double>(z_values, output_full);
      
      /*
       * teile den Vektor z_values in seine s=6 Teile der Dimension n=2
       */
      Vector<double> z_1(odesize);	
      Vector<double> z_2(odesize);
      Vector<double> z_3(odesize);
      Vector<double> z_4(odesize);
      Vector<double> z_5(odesize); 
      Vector<double> z_6(odesize);

      for (int i = 0; i < odesize; i++)
      {
	z_1(i) = z_values(i);
	z_2(i) = z_values(i+odesize);
	z_3(i) = z_values(i+(2*odesize));
	z_4(i) = z_values(i+(3*odesize));
	z_5(i) = z_values(i+(4*odesize));
	z_6(i) = z_values(i+(5*odesize));
      }
      
      /*
       * berechne neue Iterierte (Dimension n=2), die alte Iterierte ist in take_prev_iterate gespeichert
       */      
      Vector<double> next_iterate;
      next_iterate = take_prev_iterate;
  
      /*
       * addiere zur alten Iterierten die Summe von d_i mal z_i (i läuft von 1 bis s=6)
       * es können immer nur zwei Vektoren in einem Schritt dazuaddiert werden
       */
      next_iterate.add(values_d_i(0), z_1, values_d_i(1), z_2);
      next_iterate.add(values_d_i(2), z_3, values_d_i(3), z_4);
      next_iterate.add(values_d_i(4), z_5, values_d_i(5), z_6);
  
      /*
       * schreibe die neue Iterierte auf *out.entry<VECTOR *>(0)
       */
      *out.entry<VECTOR *>(0)= next_iterate;	
      
      /*
       * Ausgabe der Werte von *out.entry<VECTOR *>(0)
       */
      if (output != 0 && control.print())
      (*output) << count << out;
      
      /*
       * berechne genaue Lösung, bei Startvektor (a 0) ist sie a*(cos(WERT*x) , sin(WERT*x))
       * werden die Werte der Problemmatrix entsprechend geändert, müssen sie in den cos/sin Funktionen auch angepasst werden
       */
      VECTOR exact_solution(odesize);
      
      exact_solution(0)=std::cos(14.14*d_implicit.time);
      exact_solution(1)=std::sin(14.14*d_implicit.time);
      //deallog << "exakte Lösung: " << exact_solution <<  std::endl;
      /*
       * berechne den Fehler
       */
      exact_solution.add(-1, *out.entry<VECTOR *>(0));
      deallog  << "Fehler: " << exact_solution << std::endl;
      
      d_explicit.time = control.now();
    }
    
    deallog.pop();
  }
  
}

DEAL_II_NAMESPACE_CLOSE





 
 

