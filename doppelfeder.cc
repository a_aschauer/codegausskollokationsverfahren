

// Dieses File hat theta_timestepping.cc als Vorlage

/*
 * Implementierung des Doppelfederpendels mit Federkonstanten k_1=40, k_2=40/3 und Massen m_1=4, m_2=1
 * Mit dem Anfangswert (1 0 0 0) entsteht ein chaotischer Lösungsverlauf
 * zugehöriges Header-File: doppelfeder.h, dort wird mit dem vierstufigen Gauß-Kollokationsverfahren gerechnet
 */

#include <deal.II/base/logstream.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>

#include <deal.II/algorithms/operator.h>
#include <deal.II/algorithms/theta_timestepping.h>
#include <deal.II/algorithms/doppelfeder.h>

#include <iostream>

using namespace dealii;
using namespace Algorithms;


class Explicit : public Operator<Vector<double> >
{
public:
  Explicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Explicit> matrix;
  FullMatrix<double> m;
};


class Implicit : public Operator<Vector<double> >
{
public:
  Implicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Implicit> matrix;
  /*
   * 2 Matrizen, da 2 Blöcke berechnet werden müssen
   */
  FullMatrix<double> m1;
  FullMatrix<double> m2;
};


// End of declarations

int main()
{
  /*
   * Problemmatrix des Doppelfederpendels, Dimension n=4
   * k_1, k_2 sind Federkonstanten, m_1, m_2 Massen der Blöcke
   */
  double k_1 = 40.;
  double k_2 = 40/3;
  double m_1 = 4.;
  double m_2 = 1.;
  FullMatrix<double> matrix(4);
  matrix(0,1) = 1.;
  matrix(1,0) = -(k_1+k_2)/m_1;
  matrix(1,2) = k_2/m_1;
  matrix(2,3) = 1.;
  matrix(3,0) = k_2/m_2;
  matrix(3,2) = -k_2/m_2;
 

  OutputOperator<Vector<double> > out;
  out.initialize_stream(std::cout << std::setprecision(15));//Ausgabe mit n Nachkommastellen
  
  /*
   * erstelle Input Matrix I x ODE-Matrix, I hat Dimension 2, Ergebnis hat Blockgröße
   */
  FullMatrix<double> input_matrix;
  input_matrix = kronecker(identity_matrix_size2, matrix);

  Explicit op_explicit(input_matrix);
  Implicit op_implicit(input_matrix);
  
  ThetaTimestepping<Vector<double> > solver(op_explicit, op_implicit);
  solver.set_output(out);
  solver.timestep_control().final(2); //Intervallende
  solver.timestep_control().start_step(0.01); //Schrittweite
  
  /*
   * Startvektor, Dimension n
   */
  Vector<double> value(4);
  value(0) = 1.;
  //value(2) = 2.;
   
  AnyData indata;
  AnyData outdata;
  outdata.add(&value, "value");
	
  solver.notify(Events::initial);
  solver(outdata, indata);
  
}


Explicit::Explicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  m.reinit(M.m(), M.n());
}


void
Explicit::operator() (AnyData &out, const AnyData &in)
{

  const double timestep = *in.read_ptr<double>("Timestep");  
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * Matrix der rechten Seite: -h(I(2x2) x B), 2x2 da Blockstruktur (allg: Blockgröße: (s*n)/2)
       * die Rechnung wird nur ein einziges Mal ausgeführt
       */
      m.equ(-timestep, *matrix);       
    }
  this->notifications.clear();
  /*
   * berechne rechte Seite = erstellte Matrix mal im Voraus bearbeitete vorherige Iterierte 
   * 2 maliger Aufruf und dadurch Berechnnung in 2 Vektoren (bei Stufananzahl s=4) der Dimension (s*n)/2
   * mit 2 Vektoren ist die Übergabe an den impliziten Löser einfacher 
   */
  m.vmult(*out.entry<Vector<double>*>(0), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 1")); 
  m.vmult(*out.entry<Vector<double>*>(1), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 2"));
  
}


Implicit::Implicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  m1.reinit(M.m(), M.n());
  m2.reinit(M.m(), M.n());
}


void
Implicit::operator() (AnyData &out, const AnyData &in)
{
  
  const double timestep = *in.read_ptr<double>("Timestep");
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * berechne Matrix des ersten Blocks der linken Seite und invertiere diese ((Block1 von A x I) + h(I kron B))
       * die Rechnung wird nur einmal ausgeführt
       */
      m1.equ(timestep, *matrix); 
      m1.add(1, *in.read_ptr<FullMatrix<double> >("Block1")); //Faktor 1, da add nach einem Faktor verlangt
      m1.gauss_jordan(); //Invertierung
      
      /*
       * berechne Matrix des zweiten Blocks der linken Seite und invertiere diese ((Block2 von A x I) + h(I kron B))
       */      
      m2.equ(timestep, *matrix); 
      m2.add(1, *in.read_ptr<FullMatrix<double> >("Block2")); 
      m2.gauss_jordan(); 
      
    }
  this->notifications.clear();
  /*
   * berechne die Werte z_stern = Matrix der linken Seite mal mit dem expliziten Löser Berechnetes
   * Berechnung wie beim expliziten Löser in zwei Vektoren
   */
  m1.vmult(*out.entry<Vector<double>*>(0),
          *in.read_ptr<Vector<double> >("Previous time 1")); 
  m2.vmult(*out.entry<Vector<double>*>(1),
          *in.read_ptr<Vector<double> >("Previous time 2"));
  
}


