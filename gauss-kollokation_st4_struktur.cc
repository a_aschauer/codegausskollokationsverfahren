// Dieses File hat theta_timestepping.cc als Vorlage
/*
 * Implementierung des Standardbeispiels
 * Mit dem Anfangswert (1 0) entsteht ein periodischer Lösungsverlauf
 * zugehöriges Header-File: gauss-kollokation_st4_struktur.h, dort wird mit dem vierstufigen Gauß-Kollokationsverfahren gerechnet
 */

#include <deal.II/base/logstream.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>

#include <deal.II/algorithms/operator.h>
#include <deal.II/algorithms/theta_timestepping.h>
#include <deal.II/algorithms/gauss-kollokation_st4_struktur.h>

#include <iostream>


using namespace dealii;
using namespace Algorithms;


class Explicit : public Operator<Vector<double> >
{
public:
  Explicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Explicit> matrix;
  FullMatrix<double> m;
};


class Implicit : public Operator<Vector<double> >
{
public:
  Implicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Implicit> matrix;
  /*
   * 2 Matrizen, da 2 Blöcke berechnet werden müssen
   */
  FullMatrix<double> m1;
  FullMatrix<double> m2;
};


// End of declarations

int main()
{
  /*
   * Problemmatrix, Dimension n=2
   */
  FullMatrix<double> matrix(2);
  matrix(0,0) = 0.;
  matrix(1,1) = 0.;
  matrix(0,1) = 14.14;
  matrix(1,0) = -14.14;
  
  OutputOperator<Vector<double> > out;
  out.initialize_stream(std::cout << std::setprecision(15));//Ausgabe mit n Nachkommastellen
  /*
   * erstelle Input Matrix I x ODE-Matrix, I hat Dimension 2 
   */
  FullMatrix<double> input_matrix;
  input_matrix = kronecker(identity_matrix_size2, matrix);

  Explicit op_explicit(input_matrix);
  Implicit op_implicit(input_matrix);
  
  ThetaTimestepping<Vector<double> > solver(op_explicit, op_implicit);
  solver.set_output(out);
  solver.timestep_control().final(5); //Intervallende 
  solver.timestep_control().start_step(0.04);//Schrittweite
 
 
  /*
   * Startvektor, Dimension n
   */
  Vector<double> value(2);
  value(0) = 1.;
 
  AnyData indata;
  AnyData outdata;
  outdata.add(&value, "value");
	
  solver.notify(Events::initial);
  solver(outdata, indata);
  
}


Explicit::Explicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  m.reinit(M.m(), M.n());
}


void
Explicit::operator() (AnyData &out, const AnyData &in)
{

  const double timestep = *in.read_ptr<double>("Timestep");  
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * Matrix der rechten Seite: -h(I(2x2) x B), 2x2 da Blockstruktur (allg: Blockgröße: (s*n)/2)
       * die Rechnung wird nur ein einziges Mal ausgeführt
       */
      m.equ(-timestep, *matrix);       
    }
  this->notifications.clear();
  /*
   * berechne rechte Seite = erstellte Matrix mal im Voraus bearbeitete vorherige Iterierte 
   * 2 maliger Aufruf und dadurch Berechnnung in 2 Vektoren der Dimension s=4
   */
  m.vmult(*out.entry<Vector<double>*>(0), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 1")); 
  m.vmult(*out.entry<Vector<double>*>(1), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 2"));
  
}


Implicit::Implicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  m1.reinit(M.m(), M.n());
  m2.reinit(M.m(), M.n());
}


void
Implicit::operator() (AnyData &out, const AnyData &in)
{
  
  const double timestep = *in.read_ptr<double>("Timestep");
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * berechne Matrix des ersten Blocks der linken Seite und invertiere diese ((Block1 von A x I) + h(I kron B))
       * die Rechnung wird nur einmal ausgeführt
       */
      m1.equ(timestep, *matrix); 
      m1.add(1, *in.read_ptr<FullMatrix<double> >("Block1")); //Faktor 1, da add nach einem Faktor verlangt
      m1.gauss_jordan(); //Invertierung
      
      /*
       * berechne Matrix des zweiten Blocks der linken Seite und invertiere diese ((Block2 von A x I) + h(I kron B))
       */      
      m2.equ(timestep, *matrix); 
      m2.add(1, *in.read_ptr<FullMatrix<double> >("Block2")); 
      m2.gauss_jordan(); 
      
    }
  this->notifications.clear();
  /*
   * berechne die Werte z_stern = Matrix der linken Seite mal mit dem expliziten Löser Berechnetes
   */
  m1.vmult(*out.entry<Vector<double>*>(0),
          *in.read_ptr<Vector<double> >("Previous time 1")); 
  m2.vmult(*out.entry<Vector<double>*>(1),
          *in.read_ptr<Vector<double> >("Previous time 2"));
  
}


