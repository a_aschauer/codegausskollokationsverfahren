
// header Datei bestehend aus angepasstem theta_timestepping_templates.h
// vierstufiges Gauß-Kollokationsverfahren 


#include <deal.II/base/parameter_handler.h>
#include <deal.II/lac/vector_memory.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/algorithms/theta_timestepping.h>


DEAL_II_NAMESPACE_OPEN

namespace Algorithms {
  template <class VECTOR>
  ThetaTimestepping<VECTOR>::ThetaTimestepping (Operator<VECTOR> &e, Operator<VECTOR> &i)
    : vtheta(0.5), adaptive(false), op_explicit(&e), op_implicit(&i)
  {}
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::notify(const Event &e)
  {
    op_explicit->notify(e);
    op_implicit->notify(e);
  }
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::declare_parameters(ParameterHandler &param)
  {
    param.enter_subsection("ThetaTimestepping");
    TimestepControl::declare_parameters (param);
    param.declare_entry("Theta", ".5", Patterns::Double());
    param.declare_entry("Adaptive", "false", Patterns::Bool());
    param.leave_subsection();
  }
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::parse_parameters (ParameterHandler &param)
  {
    param.enter_subsection("ThetaTimestepping");
    control.parse_parameters (param);
    vtheta = param.get_double("Theta");
    adaptive = param.get_bool("Adaptive");
    param.leave_subsection ();
  }

  /*
   * Funktion zur Berechnung des Kronecker Produkts zweier FullMatrix Objekte
   */
  FullMatrix<double> kronecker(const FullMatrix<double> &a, const FullMatrix<double> &b) 
  {
    FullMatrix<double> axb(a.m()*b.m(), a.n()*b.n());

    for(unsigned int i=0; i<a.m(); ++i)
      for(unsigned int j=0; j<a.n(); ++j)
	 axb.add(b, a(i,j), i*b.m(), j*b.n());

    return axb;
  }
  
  /*
   * Einheitsmatrix der Dimension n=2
   */
  FullMatrix<double> identity_matrix_size2(IdentityMatrix(2));
 
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::operator() (AnyData &out, const AnyData &in)
  {
    Assert(!adaptive, ExcNotImplemented());
    
    deallog.push ("Gauss-Kollokation_St4");   
    deallog.precision(14);//Ausgabe mit n Nachkommastellen
    
    int odesize = 4; //Dimension der ODE-Matrix
    FullMatrix<double> identity_matrix_ODEsize(IdentityMatrix(4)); 
    
    /*
     * Werte des Gauß-Kollokationsverfahrens (s=4)
     * berechnet mit Octave
     */
      
    /*
     * Transformationsmatrix T der Jordannormalform von A_invers
     */
    FullMatrix<double> jordan_trafo_matrix(4);
    jordan_trafo_matrix(0,0) = 0.045676938944743;
    jordan_trafo_matrix(0,1) = 0.005596900829319;
    jordan_trafo_matrix(0,2) = 0.012056498009687;
    jordan_trafo_matrix(0,3) = 0.022953821305622;
    jordan_trafo_matrix(1,0) = -0.113479232531704;
    jordan_trafo_matrix(1,1) = 0.036496686603580;
    jordan_trafo_matrix(1,2) = -0.027865866968751;
    jordan_trafo_matrix(1,3) = 0.011762625061478;
    jordan_trafo_matrix(2,0) = 0.089973980766135;
    jordan_trafo_matrix(2,1) = -0.339449808488173;
    jordan_trafo_matrix(2,2) = -0.179267614998285;
    jordan_trafo_matrix(2,3) = 0.109402282673109;
    jordan_trafo_matrix(3,0) = 0.927551215784421;
    jordan_trafo_matrix(3,1) = 0.;
    jordan_trafo_matrix(3,2) = -0.976886462088453;
    jordan_trafo_matrix(3,3) = 0.;
   

    /*
     * berechne das Kronecker Produkt der Transformationsmatrix und Einheitsmatrix der Dimension n=4
     * T x I
     */
    FullMatrix<double> trafo_matrix_kron_id;
    trafo_matrix_kron_id = kronecker(jordan_trafo_matrix, identity_matrix_ODEsize);
      
    /*
     * invertiere die Transformationsmatrix
     */
    FullMatrix<double> invers_trafo_matrix(4);
    invers_trafo_matrix.invert<double>(jordan_trafo_matrix);
    
    /*
     * berechne das Kronecker Produkt der Inversen der Transformationsmatrix und Einheitsmatrix der Dimension n=4
     * T^(-1) x I
     */
    FullMatrix<double> invers_trafo_matrix_kron_id;
    invers_trafo_matrix_kron_id = kronecker(invers_trafo_matrix, identity_matrix_ODEsize);
    
    /*
     * Jordannormalform der Inversen der RK-Matrix A, nicht aufgeführte Werte sind 0
     */
    
    FullMatrix<double> jordan_A_invers(4);
    jordan_A_invers(0,0) = 4.20757879435925;
    jordan_A_invers(0,1) = 5.31483608371350;
    jordan_A_invers(1,0) = -5.31483608371350;
    jordan_A_invers(1,1) = 4.20757879435925;
    jordan_A_invers(2,2) = 5.79242120564075;
    jordan_A_invers(2,3) = 1.73446825786900;
    jordan_A_invers(3,2) = -1.73446825786900;
    jordan_A_invers(3,3) = 5.79242120564075;
   
    /*
     * berechne das Kronecker Produkt der JNF von A_invers und Einheitsmatrix der Dimension n=4
     */
    FullMatrix<double> infAj_kron_id;
    infAj_kron_id = kronecker(jordan_A_invers, identity_matrix_ODEsize);
    
    /*
     * lese beide 2nx2n Blöcke des gerade berechneten Kronecker Produkts aus (hier 8x8)
     */
    FullMatrix<double> block1(2*odesize);
    FullMatrix<double> block2(2*odesize);
    
    for (int i = 0; i < 2*odesize; i++)
    {
      for (int j =0; j < 2*odesize; j++)
      {
	block1(i,j) = infAj_kron_id(i,j);
	block2(i,j) = infAj_kron_id(i+(2*odesize),j+(2*odesize));
      }
    }

    /*
     * Werte d_i zur Berechnung der neuen Iterierten 
     * d = RK_Gewichte*A_invers
     */
    Vector<double> values_d_i(4);
    values_d_i(0) = -1.64070532173921;
    values_d_i(1) = 1.21439396979856;
    values_d_i(2) = -1.21439396979857;
    values_d_i(3) = 1.64070532173926;

    /*
     * Vektoren, die zu den AnyData Objekten hinzugefügt werden
     * die Pointer-Struktur ist von Theta_Timestepping übernommen
     */

    /*
     * Vektor, der den Startwert bekommt und der später immer die Lösung des expliziten Teils (erster 2n dimensionaler Vektor) beinhaltet
     */
    VECTOR &solution = *out.entry<VECTOR *>(0);
    GrowingVectorMemory<VECTOR> mem1;
    typename VectorMemory<VECTOR>::Pointer aux1(mem1);
    aux1->reinit(solution);
   
    /* Vektor, in dem der zweite berechnete 2n dimensionalen Vektor des expliziten Teils gespeichert wird (Dimension hier 8)
     */
    VECTOR store(2*odesize);
    GrowingVectorMemory<VECTOR> mem2;
    typename VectorMemory<VECTOR>::Pointer aux2(mem2);
    aux2->reinit(store);
    
    /*
     * 2 Vektoren, in denen die beiden Teile der mit T^-1 x I multiplizierten alten Iterierten gespeichert werden
     * Größe der Vektoren ist egal, da sie sowieso mit Vektoren der passenden Größe überschrieben werden
     */
    VECTOR store_shift1(2);
    GrowingVectorMemory<VECTOR> mem_shift1;
    typename VectorMemory<VECTOR>::Pointer aux_shift1(mem_shift1);
    aux_shift1->reinit(store_shift1);
    
    VECTOR store_shift2(2);
    GrowingVectorMemory<VECTOR> mem_shift2;
    typename VectorMemory<VECTOR>::Pointer aux_shift2(mem_shift2);
    aux_shift2->reinit(store_shift2);
    
    /*
     * erstelle zwei Vektoren um später die Z_Stern-Teilvektoren zu speichern, die der implizite Teil berechnet
     * Vektoren gleich auf Dimension 2n setzen, dann muss die Dimension nicht mehr angepasst werden (Dimension hier 8)
     */
    VECTOR output_zstar1(2*odesize);
    GrowingVectorMemory<VECTOR> mem_z_star1;
    typename VectorMemory<VECTOR>::Pointer aux_z_star1(mem_z_star1);
    aux_z_star1->reinit(output_zstar1);
    
    VECTOR output_zstar2(2*odesize);
    GrowingVectorMemory<VECTOR> mem_z_star2;
    typename VectorMemory<VECTOR>::Pointer aux_z_star2(mem_z_star2);
    aux_z_star2->reinit(output_zstar2);
    
    control.restart();
    
    d_explicit.time = control.now();
      
    	
    /*
     * in src1 stehen die Daten, die benutzt werden um den expliziten Teil zu berechnen
     */
    AnyData src1;
    /*
     * die ersten beiden Vektoren werden später mit der davor mit T^-1 x I multiplizierten und in Vektoren der Dimension 2n geteilten alten Iterierten befüllt
     * damit das überschreiben klappt dürfen die Vektoren nicht const sein
     */
    src1.add<VECTOR *>(aux_shift1, "shifted prev iterate 1");
    src1.add<VECTOR *>(aux_shift2, "shifted prev iterate 2");
    src1.add<const VECTOR *>(&solution, "Previous iterate");
    src1.add<const double *>(&d_explicit.time, "Time");
    src1.add<const double *>(&d_explicit.step, "Timestep");
    
    /*
     * Output des expliciten Lösers
     */
    AnyData out1;
    out1.add<VECTOR *>(aux1, "Solution");
    out1.add<VECTOR *>(aux2, "Solution2");

    /*
     * src2: Daten für den impliziten Teil
     */
    AnyData src2;
    /*
     * ersten beiden Vektoren bekommen das im expliziten Teil Berechnete
     */
    src2.add<const VECTOR *>(aux1, "Previous time 1"); 
    src2.add<const VECTOR *>(aux2, "Previous time 2"); 
    /*
     * Übergabe der Matrix Blöcke 
     */
    src2.add<FullMatrix<double> *>(&block1, "Block1"); 
    src2.add<FullMatrix<double> *>(&block2, "Block2"); 
    src2.add<const double *>(&d_implicit.time, "Time");
    src2.add<const double *>(&d_implicit.step, "Timestep");
        
    /*
     * Output des impliziten Lösers, mit dem dann weitergearbeitet wird
     */
    AnyData out_zstar;
    out_zstar.add<VECTOR *>(aux_z_star1, "Z stern1");
    out_zstar.add<VECTOR *>(aux_z_star2, "Z stern2");
    
    if (output != 0)
    (*output) << 0U << out;
    
    /*
     * for Schleife, die zuerst die alte Iterierte mit T^-1 x I multipliziert, den expliziten und impliziten Löser aufruft und dann aus den berechneten Vektoren die neue Iterierte berechnet
     */    
    for (unsigned int count = 1; d_explicit.time < control.final(); ++count)
    {
      const bool step_change = control.advance(); //contol.now wird um einmal stepsize erhöht
      d_implicit.time = control.now(); //d_implicit.time wird um einmal stepsize erhöht
      //default stepsize: 0.01
      d_explicit.step = control.step();
      d_implicit.step = control.step();
      
     
      deallog << "Timestep:" << d_implicit.time << std::endl;
            
     
      op_explicit->notify(Events::new_time);
      op_implicit->notify(Events::new_time);
      if (step_change)
	{
	  op_explicit->notify(Events::new_timestep_size);
	  op_implicit->notify(Events::new_timestep_size);
	}
	

      /*
       * speichere die alte Iterierte für spätere Berechnungen
       */
      VECTOR take_prev_iterate = *src1.entry<const VECTOR *>(2);
           
      /*
       * verlängere die alte Iterierte auf die Dimension s*n = 16
       * dazu wird sie s mal untereinander geschrieben
       */
      VECTOR long_prev_iterate(4*odesize);
      for (int i = 0; i < odesize; i++)
      {
	long_prev_iterate(i) = take_prev_iterate(i);
	long_prev_iterate(i+odesize) = take_prev_iterate(i);
	long_prev_iterate(i+(2*odesize)) = take_prev_iterate(i);
	long_prev_iterate(i+(3*odesize)) = take_prev_iterate(i);
      }

      /*
       * berechne T^-1 x I mal die verlängerte alte Iterierte, teile das Ergebnis in zwei VeKtoren der Dimension 2n und schreibe sie in src1 (Dimension hier 8)
       */ 
      VECTOR shifted_prev_it(4*odesize);
      invers_trafo_matrix_kron_id.vmult<double>(shifted_prev_it, long_prev_iterate);
     
      VECTOR shifted_prev_it1(2*odesize);
      VECTOR shifted_prev_it2(2*odesize);

      for (int i = 0; i < 2*odesize; i++)
      {
	shifted_prev_it1(i) = shifted_prev_it(i);
	shifted_prev_it2(i) = shifted_prev_it(i+(2*odesize));
      }
      
      //speichern:
      *src1.entry<VECTOR *>(0) = shifted_prev_it1; 
      *src1.entry<VECTOR *>(1) = shifted_prev_it2; 
      
      /*
       * passe die Dimension der Vektoren an, die noch nicht stimmt (nur der Vektor, in den der erste 2n dimensionale Teil des Ergebnisses des expliziten Teils gespeichert wird)
       * kann nicht früher gemacht werden, da dort auch immer die vorherige Iterierte gespeichert wird
       */ 
      VECTOR long_solution(2*odesize);
      *out1.entry<VECTOR *>(0) = long_solution;
     
     /*
      * rufe expliziten und impliziten Löser auf
      */
      (*op_explicit)(out1, src1);      
      (*op_implicit)(out_zstar, src2);
      
      
      /*
       * speichere die Ausgabe des impliziten Teils, um dann damit weiterzurechnen
       */
      VECTOR output_block1 = *out_zstar.entry<VECTOR *>(0);
      VECTOR output_block2 = *out_zstar.entry<VECTOR *>(1);
      
      /*
       * erstelle mit den Werten einen Vektor der Dimension s*n=16, um diesen mit TxI zu multiplizieren
       */
      VECTOR output_full(4*odesize);

      for (int i = 0; i < 2*odesize; i++)
      {
	output_full(i) = output_block1(i);
	output_full(i+(2*odesize)) = output_block2(i);
      }

      /*
       * multipliziere mit T x I, speichere das Ergebnis in z_values
       */
      VECTOR z_values(4*odesize);
      trafo_matrix_kron_id.vmult<double>(z_values, output_full);
      
      /*
       * teile den Vektor z_values in seine s=4 Teile der Dimension n=4 (odesize)
       */
      Vector<double> z_1(odesize);   
      Vector<double> z_2(odesize);     
      Vector<double> z_3(odesize);      
      Vector<double> z_4(odesize);

      for (int i = 0; i < odesize; i++)
      {
	z_1(i) = z_values(i);
	z_2(i) = z_values(i+odesize);
	z_3(i) = z_values(i+(2*odesize));
	z_4(i) = z_values(i+(3*odesize));
      }
      
      /*
       * berechne neue Iterierte (Dimension n=4), die alte Iterierte ist in take_prev_iterate gespeichert
       */      
      Vector<double> next_iterate;
      next_iterate = take_prev_iterate;
  
      /*
       * addiere zur alten Iterierten die Summe von d_i mal z_i (i läuft von 1 bis s=4)
       * es können immer nur zwei Vektoren in einem Schritt dazuaddiert werden
       */
      next_iterate.add(values_d_i(0), z_1, values_d_i(1), z_2);
      next_iterate.add(values_d_i(2), z_3, values_d_i(3), z_4);
  
      /*
       * schreibe die neue Iterierte auf *out.entry<VECTOR *>(0)
       */
      *out.entry<VECTOR *>(0)= next_iterate;	
      
      /*
       * Ausgabe der Werte von *out.entry<VECTOR *>(0)
       */
      if (output != 0 && control.print())
      (*output) << count << out;
      
      /* Berechnung der Energie im System
       * Energie sollte immer konstant bleiben, da ungedämpfte Schwingung
       * potentielle Energie: 0.5*Federkonstante*(Auslenkung zum Quadrat), Auslenkung pro Feder
       * kinetische Energie: 0.5*Masse*(Geschwindigkeit zum Quadrat), Geschwindigkeiten sind Lösungskomponenten 2 und 4
       * jeweils bei beiden Feder-Masse Einheiten addieren
       */
     
      double k_1 = 40.;//Federkonstante 1
      double k_2 = 40/3;//Federkonstante 2
      double m_1 = 4.;//Masse 1
      double m_2 = 1.;//Masse 2
      
      double Energie = 0.5*(k_1*next_iterate(0)*next_iterate(0)+k_2*(next_iterate(2)-next_iterate(0))*(next_iterate(2)-next_iterate(0))+m_1*next_iterate(1)*next_iterate(1)+m_2*next_iterate(3)*next_iterate(3));
      
      std::cout << "Energie: " << Energie << std::endl;
      
      d_explicit.time = control.now();
    }
    
    deallog.pop();
  }
  
}

DEAL_II_NAMESPACE_CLOSE




