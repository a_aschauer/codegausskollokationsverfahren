// Dieses File hat theta_timestepping.cc als Vorlage
/*
 * Implementierung des Standardbeispiels
 * Mit dem Anfangswert (1 0) entsteht ein periodischer Lösungsverlauf
 * zugehöriges Header-File: gauss-kollokation_st5_struktur.h, dort wird mit dem fünfstufigen Gauß-Kollokationsverfahren gerechnet
 */

#include <deal.II/base/logstream.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>

#include <deal.II/algorithms/operator.h>
#include <deal.II/algorithms/theta_timestepping.h>
#include <deal.II/algorithms/gauss-kollokation_st5_struktur.h>

#include <iostream>

using namespace dealii;
using namespace Algorithms;


class Explicit : public Operator<Vector<double> >
{
public:
  Explicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Explicit> matrix;
  /*
   * m1 für kleineren Block
   * m2 für größere Blöcke
   */
  FullMatrix<double> m1;
  FullMatrix<double> m2;
};


class Implicit : public Operator<Vector<double> >
{
public:
  Implicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Implicit> matrix;
  /*
   * 3 Matrizen, da 3 Blöcke berechnet werden müssen
   * m1 für kleineren Block, m2 und m3 für größere Blöcke
   */
  FullMatrix<double> m1;
  FullMatrix<double> m2;
  FullMatrix<double> m3;
};


// End of declarations

int main()
{
  /*
   * Problemmatrix, Dimension n
   */
  FullMatrix<double> matrix(2);
  matrix(0,0) = 0.;
  matrix(1,1) = 0.;
  matrix(0,1) = 14.14;
  matrix(1,0) = -14.14;

  
  OutputOperator<Vector<double> > out;
  out.initialize_stream(std::cout << std::setprecision(15));//Ausgabe mit n Nachkommastellen

  Explicit op_explicit(matrix);
  Implicit op_implicit(matrix);
  
  ThetaTimestepping<Vector<double> > solver(op_explicit, op_implicit);
  solver.set_output(out);
  solver.timestep_control().final(5); //Intervallende
  solver.timestep_control().start_step(0.04); //Schrittweite
  
  /*
   * Startvektor, Dimension n
   */
  Vector<double> value(2);
  value(0) = 1.;
  
 
  AnyData indata;
  AnyData outdata;
  outdata.add(&value, "value");
	
  solver.notify(Events::initial);
  solver(outdata, indata);
  
}


Explicit::Explicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  /*
   * erstelle Matrizen der richtigen Größe: m1 wie ODE-Größe, m2 2mal ODE-Größe, um den Blockgrößen zu entsprechen
   */
  m1.reinit(M.m(), M.n());
  m2.reinit(2*M.m(), 2*M.n());
}


void
Explicit::operator() (AnyData &out, const AnyData &in)
{

  const double timestep = *in.read_ptr<double>("Timestep");  
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * Matrix der rechten Seite: -hB beim kleinen Block, (-h(I(2x2) x B), bei den größeren Blöcken (m2)
       * für m2 muss zuerst die ODE Matrix durch das Kronecker-Produkt mit der Einheitsmatrix vergrößert werden
       * die Rechnungen werden nur ein einziges Mal ausgeführt
       */
      m1.equ(-timestep, *matrix); 
      FullMatrix<double> bigmatrix;
      bigmatrix = kronecker(identity_matrix_size2, *matrix);
      m2.equ(-timestep, bigmatrix);      
    }
  this->notifications.clear();
  /*
   * berechne rechte Seite = erstellte Matrix mal im Voraus bearbeitete vorherige Iterierte 
   * 3 maliger Aufruf und dadurch Berechnnung in 3 Vektoren der Dimension 2 (Vektor1), bzw. Dimension 4 (Vektoren 2 und 3)
   */
  m1.vmult(*out.entry<Vector<double>*>(0), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 1")); 
  m2.vmult(*out.entry<Vector<double>*>(1), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 2"));
  m2.vmult(*out.entry<Vector<double>*>(2), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 3"));  
  
}


Implicit::Implicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  /*
   * erstelle Matrizen der richtigen Größe: m1 wie ODE-Größe, m2 und m3 2mal ODE-Größe 
   */
  m1.reinit(M.m(), M.n());
  m2.reinit(2*M.m(), 2*M.n());
  m3.reinit(2*M.m(), 2*M.n());
}


void
Implicit::operator() (AnyData &out, const AnyData &in)
{
  
  const double timestep = *in.read_ptr<double>("Timestep");
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * die Rechnungen werden nur einmal ausgeführt
       * berechne Matrix des ersten Blocks der linken Seite und invertiere diese ((Block1 von A x I) + h(I kron B))
       * Dimension 2
       */
      m1.equ(timestep, *matrix); 
      m1.add(1, *in.read_ptr<FullMatrix<double> >("Block1")); //Faktor 1, da add nach einem Faktor verlangt
      m1.gauss_jordan(); //Invertierung

      /*
       * erstelle aus ODE-Matrix größere Matrix über Kronecker Produkt mit Einheitsmatrix
       * berechne Matrix des zweiten Blocks der linken Seite und invertiere diese ((Block2 von A x I) + h(I kron B))
       * Dimension 4
       */  
      FullMatrix<double> bigmatrix;
      bigmatrix = kronecker(identity_matrix_size2, *matrix);
      
      m2.equ(timestep, bigmatrix); 
      m2.add(1, *in.read_ptr<FullMatrix<double> >("Block2")); 
      m2.gauss_jordan(); 
      
      /*
       * dritter Block, Dimension 4
       */
      m3.equ(timestep, bigmatrix); 
      m3.add(1, *in.read_ptr<FullMatrix<double> >("Block3")); 
      m3.gauss_jordan(); 


            
    }
  this->notifications.clear();
  /*
   * berechne die Werte z_stern = Matrix der linken Seite mal mit dem expliziten Löser Berechnetes
   */
  m1.vmult(*out.entry<Vector<double>*>(0),
          *in.read_ptr<Vector<double> >("Previous time 1")); //Dimension 2
  m2.vmult(*out.entry<Vector<double>*>(1),
          *in.read_ptr<Vector<double> >("Previous time 2")); //Dimension 4
  m3.vmult(*out.entry<Vector<double>*>(2),
          *in.read_ptr<Vector<double> >("Previous time 3")); //Dimension 4
}


