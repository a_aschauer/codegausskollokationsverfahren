// Dieses File hat theta_timestepping.cc als Vorlage

/*
 * Implementierung des Standardbeispiels
 * Mit dem Anfangswert (1 0) entsteht ein periodischer Lösungsverlauf
 * zugehöriges Header-File: gauss-kollokation_st6_struktur.h, dort wird mit dem sechsstufigen Gauß-Kollokationsverfahren gerechnet
 */

#include <deal.II/base/logstream.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>

#include <deal.II/algorithms/operator.h>
#include <deal.II/algorithms/theta_timestepping.h>
#include <deal.II/algorithms/gauss-kollokation_st6_struktur.h>

#include <iostream>

using namespace dealii;
using namespace Algorithms;


class Explicit : public Operator<Vector<double> >
{
public:
  Explicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Explicit> matrix;
  FullMatrix<double> m;
};


class Implicit : public Operator<Vector<double> >
{
public:
  Implicit(const FullMatrix<double> &matrix);
  void operator() (AnyData &out, const AnyData &in);

private:
  SmartPointer<const FullMatrix<double>, Implicit> matrix;
  /*
   * 3 Matrizen, da 3 Blöcke berechnet werden müssen
   */
  FullMatrix<double> m1;
  FullMatrix<double> m2;
  FullMatrix<double> m3;
};


// End of declarations

int main()
{
  /*
   * Problemmatrix, Dimension n=2
   */
  FullMatrix<double> matrix(2);
  matrix(0,0) = 0.;
  matrix(1,1) = 0.;
  matrix(0,1) = 14.14;
  matrix(1,0) = -14.14;



  OutputOperator<Vector<double> > out;
  out.initialize_stream(std::cout << std::setprecision(16));//Ausgabe mit n Nachkommastellen
    
  /*
   * erstelle Input Matrix I x ODE-Matrix, I hat Dimension 2 
   */
  FullMatrix<double> input_matrix;
  input_matrix = kronecker(identity_matrix_size2, matrix);

  Explicit op_explicit(input_matrix);
  Implicit op_implicit(input_matrix);
  
  ThetaTimestepping<Vector<double> > solver(op_explicit, op_implicit);
  solver.set_output(out);
  solver.timestep_control().final(5); //Intervallende
  solver.timestep_control().start_step(0.04);//Schrittweite
  
  /*
   * Startvektor, Dimension n
   */
  Vector<double> value(2);
  value(0) = 1.;  
 
  AnyData indata;
  AnyData outdata;
  outdata.add(&value, "value");
	
  solver.notify(Events::initial);
  solver(outdata, indata);
  
}


Explicit::Explicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  m.reinit(M.m(), M.n());
}


void
Explicit::operator() (AnyData &out, const AnyData &in)
{

  const double timestep = *in.read_ptr<double>("Timestep");  
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * Matrix der rechten Seite: -h(I(2x2) x B), 2x2 da Blockstruktur (allg: Blockgröße: (s*n)/2)
       * die Rechnung wird nur ein einziges Mal ausgeführt
       */
      m.equ(-timestep, *matrix);       
    }
  this->notifications.clear();
  /*
   * berechne rechte Seite = erstellte Matrix mal im Voraus bearbeitete vorherige Iterierte 
   * 3 maliger Aufruf und dadurch Berechnnung in 3 Vektoren der Dimension 4
   */
  m.vmult(*out.entry<Vector<double>*>(0), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 1")); 
  m.vmult(*out.entry<Vector<double>*>(1), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 2"));
  m.vmult(*out.entry<Vector<double>*>(2), 
          *in.read_ptr<Vector<double> >("shifted prev iterate 3"));  
  
}


Implicit::Implicit(const FullMatrix<double> &M)
  :
  matrix(&M)
{
  m1.reinit(M.m(), M.n());
  m2.reinit(M.m(), M.n());
  m3.reinit(M.m(), M.n());
}


void
Implicit::operator() (AnyData &out, const AnyData &in)
{
  
  const double timestep = *in.read_ptr<double>("Timestep");
  if (this->notifications.test(Events::initial) || this->notifications.test(Events::new_timestep_size))
    {
      /*
       * berechne Matrix des ersten Blocks der linken Seite und invertiere diese ((Block1 von A x I) + h(I kron B))
       * die Rechnungen werden nur einmal ausgeführt
       */
      m1.equ(timestep, *matrix); 
      m1.add(1, *in.read_ptr<FullMatrix<double> >("Block1")); //Faktor 1, da add nach einem Faktor verlangt
      m1.gauss_jordan(); //Invertierung
      
      /*
       * berechne Matrix des zweiten Blocks der linken Seite und invertiere diese ((Block2 von A x I) + h(I kron B))
       */      
      m2.equ(timestep, *matrix); 
      m2.add(1, *in.read_ptr<FullMatrix<double> >("Block2")); 
      m2.gauss_jordan(); 
      
      /*
       * dritter Block
       */
      m3.equ(timestep, *matrix); 
      m3.add(1, *in.read_ptr<FullMatrix<double> >("Block3")); 
      m3.gauss_jordan(); 
      
    }
  this->notifications.clear();
  /*
   * berechne die Werte z_stern = Matrix der linken Seite mal mit dem expliziten Löser Berechnetes
   */
  m1.vmult(*out.entry<Vector<double>*>(0),
          *in.read_ptr<Vector<double> >("Previous time 1")); 
  m2.vmult(*out.entry<Vector<double>*>(1),
          *in.read_ptr<Vector<double> >("Previous time 2"));
  m3.vmult(*out.entry<Vector<double>*>(2),
          *in.read_ptr<Vector<double> >("Previous time 3"));
}


