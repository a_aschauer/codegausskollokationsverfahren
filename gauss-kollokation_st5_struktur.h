 
// header datei bestehend aus angepasstem theta_timestepping_templates.h
// fünfstufiges Gauß-Kollokationsverfahren


#include <deal.II/base/parameter_handler.h>
#include <deal.II/lac/vector_memory.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/algorithms/theta_timestepping.h>


DEAL_II_NAMESPACE_OPEN

namespace Algorithms {
  template <class VECTOR>
  ThetaTimestepping<VECTOR>::ThetaTimestepping (Operator<VECTOR> &e, Operator<VECTOR> &i)
    : vtheta(0.5), adaptive(false), op_explicit(&e), op_implicit(&i)
  {}
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::notify(const Event &e)
  {
    op_explicit->notify(e);
    op_implicit->notify(e);
  }
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::declare_parameters(ParameterHandler &param)
  {
    param.enter_subsection("ThetaTimestepping");
    TimestepControl::declare_parameters (param);
    param.declare_entry("Theta", ".5", Patterns::Double());
    param.declare_entry("Adaptive", "false", Patterns::Bool());
    param.leave_subsection();
  }
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::parse_parameters (ParameterHandler &param)
  {
    param.enter_subsection("ThetaTimestepping");
    control.parse_parameters (param);
    vtheta = param.get_double("Theta");
    adaptive = param.get_bool("Adaptive");
    param.leave_subsection ();
  }

  /*
   * Funktion zur Berechnung des Kronecker Produkts zweier FullMatrix Objekte
   */
  FullMatrix<double> kronecker(const FullMatrix<double> &a, const FullMatrix<double> &b) 
  {
    FullMatrix<double> axb(a.m()*b.m(), a.n()*b.n());

    for(unsigned int i=0; i<a.m(); ++i)
      for(unsigned int j=0; j<a.n(); ++j)
	 axb.add(b, a(i,j), i*b.m(), j*b.n());

    return axb;
  }
  
  /*
   * Einheitsmatrix der Dimension n=2
   */
  FullMatrix<double> identity_matrix_size2(IdentityMatrix(2));
  
  
  template <class VECTOR>
  void
  ThetaTimestepping<VECTOR>::operator() (AnyData &out, const AnyData &in)
  {
    Assert(!adaptive, ExcNotImplemented());
    
    deallog.push ("Gauss-Kollokation_St5");    
    deallog.precision(15);
    
    int odesize = 2; //Dimension der ODE-Matrix
    
    /*
     * Werte des Gauß-Kollokationsverfahrens (s=5)
     * berechnet mit Octave
     */
      
    /*
     * Transformationsmatrix T der Jordannormalform von A_invers
     */
    FullMatrix<double> jordan_trafo_matrix(5);
    jordan_trafo_matrix(0,0) = -0.009030627445916;
    jordan_trafo_matrix(0,1) = -0.017761143010427;
    jordan_trafo_matrix(0,2) = -0.018320263073171;
    jordan_trafo_matrix(0,3) = -0.004608669280731;
    jordan_trafo_matrix(0,4) = 0.010521756143669;
    jordan_trafo_matrix(1,0) = 0.002767887556224;
    jordan_trafo_matrix(1,1) = 0.049645019802475;
    jordan_trafo_matrix(1,2) = 0.029204111659185;
    jordan_trafo_matrix(1,3) = -0.005768162259977;
    jordan_trafo_matrix(1,4) = -0.015214920129764;
    jordan_trafo_matrix(2,0) = -0.042697767969385;
    jordan_trafo_matrix(2,1) = -0.144269176728558;
    jordan_trafo_matrix(2,2) = 0.011719339710654;
    jordan_trafo_matrix(2,3) = -0.000765823325553;
    jordan_trafo_matrix(2,4) = -0.035728186480683;
    jordan_trafo_matrix(3,0) = -0.244848729198133;
    jordan_trafo_matrix(3,1) = 0.130011306043385;
    jordan_trafo_matrix(3,2) = -0.375165546158977;
    jordan_trafo_matrix(3,3) = 0.223551348504571;
    jordan_trafo_matrix(3,4) = -0.178414679469545;
    jordan_trafo_matrix(4,0) = -0.968574616117365;
    jordan_trafo_matrix(4,1) = 0.904117435198461;
    jordan_trafo_matrix(4,2) = 0.;
    jordan_trafo_matrix(4,3) = 0.957350089706845;
    jordan_trafo_matrix(4,4) = 0.;
    
    /*
     * berechne das Kronecker Produkt der Transformationsmatrix und Einheitsmatrix der Dimension n=2
     * T x I
     */
    FullMatrix<double> trafo_matrix_kron_id;
    trafo_matrix_kron_id = kronecker(jordan_trafo_matrix, identity_matrix_size2);
      
    /*
     * invertiere die Transformationsmatrix
     */
    FullMatrix<double> invers_trafo_matrix(5);
    invers_trafo_matrix.invert<double>(jordan_trafo_matrix);
    
    /*
     * berechne das Kronecker Produkt der Inversen der Transformationsmatrix und Einheitsmatrix der Dimension n=2
     * T^(-1) x I
     */
    FullMatrix<double> invers_trafo_matrix_kron_id;
    invers_trafo_matrix_kron_id = kronecker(invers_trafo_matrix, identity_matrix_size2);
    
    /*
     * Jordannormalform der Inversen der RK-Matrix A, zuerst der reelle Eigenwert
     */
    FullMatrix<double> jordan_A_invers(5);
    jordan_A_invers(0,0) = 7.29347719065927;
    jordan_A_invers(1,1) = 4.64934860636330;
    jordan_A_invers(1,2) = 7.14204584067598;
    jordan_A_invers(2,1) = -7.14204584067598;
    jordan_A_invers(2,2) = 4.64934860636330;
    jordan_A_invers(3,3) = 6.70391279830709;
    jordan_A_invers(3,4) = 3.48532283236642;
    jordan_A_invers(4,3) = -3.48532283236642;
    jordan_A_invers(4,4) = 6.70391279830709;
    
    /*
     * berechne das Kronecker Produkt der JNF von A_invers und Einheitsmatrix der Dimension n=2
     */
    FullMatrix<double> infAj_kron_id;
    infAj_kron_id = kronecker(jordan_A_invers, identity_matrix_size2);
    
    /*
     * lese den ersten nxn Block des gerade berechneten Kronecker Produkts aus (Block passend zum reellen EW)
     */
    FullMatrix<double> block1(odesize);
    block1(0,0) = infAj_kron_id(0,0);
    block1(1,1) = infAj_kron_id(1,1);
    /*
     * lese beide 2nx2n Blöcke aus
     */
    FullMatrix<double> block2(2*odesize);
    FullMatrix<double> block3(2*odesize);
    
    for (int i = 0; i < 2*odesize; i++)
    {
      for (int j =0; j < 2*odesize; j++)
      {
	block2(i,j) = infAj_kron_id(i+odesize,j+odesize);
	block3(i,j) = infAj_kron_id(i+(3*odesize),j+(3*odesize));
      }
    }
    
    /*
     * Werte d_i zur Berechnung der neuen Iterierten 
     * Reihenfolge darf nicht verändert werden, auch wenn die Eigenwerte in der JNF vertauscht wurden
     */
    Vector<double> values_d_i(5);
    values_d_i(0) = 1.62776671089016;
    values_d_i(1) = -1.16110004422349;
    values_d_i(2) = 1.06666666666669;
    values_d_i(3) = -1.16110004422348;
    values_d_i(4) = 1.62776671089013; 

   
    /*
     * Vektoren, die zu den AnyData Objekten hinzugefügt werden
     * die Pointer-Struktur ist von Theta_Timestepping übernommen
     */

    /*
     * Vektor, der den Startwert bekommt und der später immer die Lösung des expliziten Teils (erster n dimensionaler Vektor des reellen EWs) beinhaltet
     */
    VECTOR &solution = *out.entry<VECTOR *>(0);
    GrowingVectorMemory<VECTOR> mem1;
    typename VectorMemory<VECTOR>::Pointer aux1(mem1);
    aux1->reinit(solution);
   
    /* 
     * Vektor, in dem der zweite berechnete 2n dimensionalen Vektor des expliziten Teils gespeichert wird (Dimension 4)
     */
    VECTOR store(2*odesize);
    GrowingVectorMemory<VECTOR> mem2;
    typename VectorMemory<VECTOR>::Pointer aux2(mem2);
    aux2->reinit(store);
    
    /*
     * dritter Vektor, Dimension 2n
     */
    VECTOR store1(2*odesize);
    GrowingVectorMemory<VECTOR> mem3;
    typename VectorMemory<VECTOR>::Pointer aux3(mem3);
    aux3->reinit(store1);
    
    /*
     * 3 Vektoren, in denen die drei Teile der mit T^-1 x I multiplizierten alten Iterierten gespeichert werden
     * Größe der Vektoren ist egal, da sie sowieso mit Vektoren der passenden Größe überschrieben werden
     */
    VECTOR store_shift1(2);
    GrowingVectorMemory<VECTOR> mem_shift1;
    typename VectorMemory<VECTOR>::Pointer aux_shift1(mem_shift1);
    aux_shift1->reinit(store_shift1);
    
    VECTOR store_shift2(2);
    GrowingVectorMemory<VECTOR> mem_shift2;
    typename VectorMemory<VECTOR>::Pointer aux_shift2(mem_shift2);
    aux_shift2->reinit(store_shift2);
    
    VECTOR store_shift3(2);
    GrowingVectorMemory<VECTOR> mem_shift3;
    typename VectorMemory<VECTOR>::Pointer aux_shift3(mem_shift3);
    aux_shift3->reinit(store_shift3);
    
    /*
     * erstelle drei Vektoren um später die Z_Stern Teilvektoren zu speichern, die der implizite Löser berechnet
     * Vektoren gleich auf Dimension n, bzw 2n setzen, dann muss die Dimension nicht mehr angepasst werden (erster Vektor Dimension 2, zweiter und dritter Dimension 4)
     */
    VECTOR output_zstar1(odesize);
    GrowingVectorMemory<VECTOR> mem_z_star1;
    typename VectorMemory<VECTOR>::Pointer aux_z_star1(mem_z_star1);
    aux_z_star1->reinit(output_zstar1);
    
    VECTOR output_zstar2(2*odesize);
    GrowingVectorMemory<VECTOR> mem_z_star2;
    typename VectorMemory<VECTOR>::Pointer aux_z_star2(mem_z_star2);
    aux_z_star2->reinit(output_zstar2);
    
    VECTOR output_zstar3(2*odesize);
    GrowingVectorMemory<VECTOR> mem_z_star3;
    typename VectorMemory<VECTOR>::Pointer aux_z_star3(mem_z_star3);
    aux_z_star3->reinit(output_zstar3);    
    
    control.restart();
    
    d_explicit.time = control.now();
         	
    /*
     * in src1 stehen die Daten, die benutzt werden um den expliziten Teil zu berechnen
     */
    AnyData src1;
    /*
     * die ersten drei Vektoren werden später mit der davor mit T^-1 x I multiplizierten und in Vektoren der Dimension n, 2n, 2n geteilten alten Iterierten befüllt
     * damit das überschreiben klappt dürfen die Vektoren nicht const sein
     */
    src1.add<VECTOR *>(aux_shift1, "shifted prev iterate 1");
    src1.add<VECTOR *>(aux_shift2, "shifted prev iterate 2");
    src1.add<VECTOR *>(aux_shift3, "shifted prev iterate 3");
    src1.add<const VECTOR *>(&solution, "Previous iterate");
    src1.add<const double *>(&d_explicit.time, "Time");
    src1.add<const double *>(&d_explicit.step, "Timestep");
    
    /*
     * Output des expliziten Lösers
     */
    AnyData out1;
    out1.add<VECTOR *>(aux1, "Solution");
    out1.add<VECTOR *>(aux2, "Solution2");
    out1.add<VECTOR *>(aux3, "Solution3");

    /*
     * src2: Daten für den impliziten Löser
     */
    AnyData src2;
    /*
     * ersten drei Vektoren bekommen das im expliziten Teil Berechnete
     */
    src2.add<const VECTOR *>(aux1, "Previous time 1"); 
    src2.add<const VECTOR *>(aux2, "Previous time 2"); 
    src2.add<const VECTOR *>(aux3, "Previous time 3"); 
    /*
     * übergebe die Matrix Blöcke 
     */
    src2.add<FullMatrix<double> *>(&block1, "Block1"); 
    src2.add<FullMatrix<double> *>(&block2, "Block2"); 
    src2.add<FullMatrix<double> *>(&block3, "Block3"); 
    src2.add<const double *>(&d_implicit.time, "Time");
    src2.add<const double *>(&d_implicit.step, "Timestep");
        
    /*
     * Output des impliziten Lösers, mit dem dann weitergearbeitet wird
     */
    AnyData out_zstar;
    out_zstar.add<VECTOR *>(aux_z_star1, "Z stern1");
    out_zstar.add<VECTOR *>(aux_z_star2, "Z stern2");
    out_zstar.add<VECTOR *>(aux_z_star3, "Z stern3");
    
    if (output != 0)
    (*output) << 0U << out;
    
    /*
     * for Schleife, die zuerst die alte Iterierte mit T^-1 x I multipliziert, den expliziten und impliziten Löser aufruft und dann aus den berechneten Vektoren die neue Iterierte berechnet
     */    
    for (unsigned int count = 1; d_explicit.time < control.final(); ++count)
    {
      const bool step_change = control.advance();
      d_implicit.time = control.now();
      //default stepsize: 0.01
      d_explicit.step = control.step();
      d_implicit.step = control.step();
      
     
      deallog << "Timestep:" << d_implicit.time << std::endl;  
     
      op_explicit->notify(Events::new_time);
      op_implicit->notify(Events::new_time);
      if (step_change)
	{
	  op_explicit->notify(Events::new_timestep_size);
	  op_implicit->notify(Events::new_timestep_size);
	}
	
      /*
       * speichere die alte Iterierte für spätere Berechnungen
       */
      VECTOR take_prev_iterate = *src1.entry<const VECTOR *>(3);
           
      /*
       * verlängere die alte Iterierte auf die Dimension s*n = 10
       */
 
      VECTOR long_prev_iterate(5*odesize);
      for (int i = 0; i < odesize; i++)
      {
	long_prev_iterate(i) = take_prev_iterate(i);
	long_prev_iterate(i+odesize) = take_prev_iterate(i);
	long_prev_iterate(i+(2*odesize)) = take_prev_iterate(i);
	long_prev_iterate(i+(3*odesize)) = take_prev_iterate(i);
	long_prev_iterate(i+(4*odesize)) = take_prev_iterate(i);
      }
      /*
       * berechne T^-1 x I mal die verlängerte alte Iterierte, teile das Ergebnis in drei Vektoren der Dimensionen n, 2n, 2n und schreibe sie in src1 (hier 2,4,4)
       */ 
      VECTOR shifted_prev_it(5*odesize);
      invers_trafo_matrix_kron_id.vmult<double>(shifted_prev_it, long_prev_iterate);
          
      VECTOR shifted_prev_it1(odesize);
      shifted_prev_it1(0) = shifted_prev_it(0);
      shifted_prev_it1(1) = shifted_prev_it(1);
      
      VECTOR shifted_prev_it2(2*odesize);
      VECTOR shifted_prev_it3(2*odesize);
  
      for (int i = 0; i < 2*odesize; i++)
      {
	shifted_prev_it2(i) = shifted_prev_it(i+odesize);
	shifted_prev_it3(i) = shifted_prev_it(i+(3*odesize));
      }
      //speichern
      *src1.entry<VECTOR *>(0) = shifted_prev_it1;   
      *src1.entry<VECTOR *>(1) = shifted_prev_it2; 
      *src1.entry<VECTOR *>(2) = shifted_prev_it3; 
      
     
     /*
      * rufe expliziten und impliziten Löser auf
      */
      (*op_explicit)(out1, src1);      
      (*op_implicit)(out_zstar, src2);
      
      
      /*
       * speichere die Ausgabe des impliziten Lösers, um dann damit weiterzurechnen
       */
      VECTOR output_block1 = *out_zstar.entry<VECTOR *>(0);
      VECTOR output_block2 = *out_zstar.entry<VECTOR *>(1);
      VECTOR output_block3 = *out_zstar.entry<VECTOR *>(2);
      
      /*
       * erstelle mit den Werten einen Vektor der Dimension s*n=10, um diesen mit TxI zu multiplizieren
       */
      VECTOR output_full(5*odesize);
      /*
       * kleiner erster Block ohne for-Schleife zuweisen
       */
      output_full(0) = output_block1(0);
      output_full(1) = output_block1(1);
      for (int i = 0; i < 2*odesize; i++)
      {
        output_full(i+odesize) = output_block2(i);
	output_full(i+(3*odesize)) = output_block3(i);
	
      }

      /*
       * multipliziere mit T x I, speichere das Ergebnis in z_values
       */
      VECTOR z_values(5*odesize);
      trafo_matrix_kron_id.vmult<double>(z_values, output_full);
      
      /*
       * teile den Vektor z_values in seine s=5 Teile der Dimension n=2
       */
      Vector<double> z_1(odesize);	
      Vector<double> z_2(odesize);
      Vector<double> z_3(odesize);
      Vector<double> z_4(odesize);
      Vector<double> z_5(odesize); 

      for (int i = 0; i < odesize; i++)
      {
	z_1(i) = z_values(i);
	z_2(i) = z_values(i+odesize);
	z_3(i) = z_values(i+(2*odesize));
	z_4(i) = z_values(i+(3*odesize));
	z_5(i) = z_values(i+(4*odesize));
      }
    
      /*
       * berechne neue Iterierte (Dimension n=2), die alte Iterierte ist in take_prev_iterate gespeichert
       */      
      Vector<double> next_iterate;
      next_iterate = take_prev_iterate;
  
      /*
       * addiere zur alten Iterierten die Summe von d_i mal z_i (i läuft von 1 bis s=5)
       * es können immer nur zwei Vektoren in einem Schritt dazuaddiert werden
       */
      next_iterate.add(values_d_i(0), z_1, values_d_i(1), z_2);
      next_iterate.add(values_d_i(2), z_3, values_d_i(3), z_4);
      next_iterate.add(values_d_i(4), z_5);
  
      /*
       * schreibe die neue Iterierte auf *out.entry<VECTOR *>(0)
       */
      *out.entry<VECTOR *>(0)= next_iterate;	
      
      /*
       * ausgabe der Werte von *out.entry<VECTOR *>(0)
       */
      if (output != 0 && control.print())
      (*output) << count << out;
      
      /*
       * berechne exakte Lösung, bei Startvektor (1 0) ist sie (cos(WERT*x) sin(WERT*x))
       * werden die Werte der Problemmatrix entsprechend geändert, müssen sie in den cos/sin Funktionen auch angepasst werden
       */
      VECTOR exact_solution(odesize);    
      exact_solution(0) = std::cos(14.14*d_implicit.time);
      exact_solution(1) = std::sin(14.14*d_implicit.time);
      //deallog  << "exakte Lösung: " << exact_solution << std::endl;
      /*
       * berechne den Fehler
       */
      exact_solution.add(-1, *out.entry<VECTOR *>(0));
      deallog  << "Fehler: " << exact_solution << std::endl;
      
      d_explicit.time = control.now();
    }
    
    deallog.pop();
  }
  
}

DEAL_II_NAMESPACE_CLOSE





 
 

